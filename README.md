# F6550 Stavba a vývoj vesmíru

V tomto repositáři se budou nacházet vybrané příklady pro cvičeni *Stavba a vývoj vesmíru*.

Čerpáno především z:

* RYDEN, Barbara Sue. *Introduction to cosmology*. San Francisco: Addison-Wesley, c2003, ix, 244 p. ISBN 08-053-8912-1. 
* LIDDLE, Andrew R. *An introduction to modern cosmology*. 2nd ed. Hoboken, NJ: Wiley, c2003, xv, 172 p. ISBN 04-708-4835-9.
* Články v A&A
* Planck Mission Archive 


