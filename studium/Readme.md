# Studium


* **Distance measures in cosmology**: *David W. Hogg*, [arXiv:astro-ph/9905116](http://arxiv.org/abs/astro-ph/9905116)
> Formulae for the line-of-sight and transverse comoving distances, proper motion distance, angular diameter distance, luminosity distance, k-correction, distance modulus, comoving volume, lookback time, age, and object intersection probability are all given, some with justifications. Some attempt is made to rationalize disparate terminologies, or at least abuse bad usage.


* **The picture of our universe: A view from modern cosmology**: *David D. Reid et al.* [arXiv:astro-ph/0209504](http://arxiv.org/abs/astro-ph/0209504)
> In this paper we give a pedagogical review of the recent observational results in cosmology from the study of type Ia supernovae and anisotropies in the cosmic microwave background. By providing consistent constrainst on the cosmological parameters, these results paint a concrete picture of our present-day universe. We present this new picture and show how it can be used to answer some of the basic questions that cosmologists have been asking for several decades. This paper is most appropriate for students of general relativity and/or relativistic cosmology.


* **Measuring and Understanding the Universe**: *Wendy L. Freedman, Michael S. Turner.* [arXiv:astro-ph/0308418](http://arxiv.org/abs/astro-ph/0308418)
> Revolutionary advances in both theory and technology have launched cosmology into its most exciting period of discovery yet. Unanticipated components of the universe have been identified, promising ideas for understanding the basic features of the universe are being tested, and deep connections between physics on the smallest scales and on the largest scales are being revealed.

