# Články


* **The gravitational lens effect**: *Sjur Refsdal* [1964MNRAS.128..295R](http://adsabs.harvard.edu/abs/1964MNRAS.128..295R)
> The so-called gravitational lens effect, previously worked out by Tikhov in 1937, is derived in a simple manner. The effect is caused by the gravitational deflection of light from a star *S* in the gravitational field of another star *B*, and occurs when *S* lies far behind *B*, but close to the line of sight throught *B*. It turns out that a considerable increase in the apparent luminosity of *S* is possible. A method is given to determine the mass of a star which acts as a gravitational lens. The possibility of observing the effect is discussed.


* **Rotation velocities of 16 SA galaxies and a comparison of Sa, Sb, and SC rotation properties**: *Rubin, V. C. et al.* [1985ApJ...289...81R](http://adsabs.harvard.edu/abs/1985ApJ...289...81R)
>Rotational velocities over most of the optical extent of 54 Sa, Sb, and Sc galaxies have been determined. The Sa curves exhibit a similar progression with luminosity as do the Sb and Sc galaxies, and the forms of the rotation curves exhibited by Sa's are markedly similar to those found previously for Sb and Sc galaxies. The overall similarity of forms of rotation curves for spirals of very different morphologies, coupled with the derived values for the dynamical mass-to-luminosity ratios, implies that both the dark halo mass and the disk mass contribute to the total mass distribution at all radii within the optical galaxy. While the approximately four mag range is similar within each Hubble type, the values of V(max) increase with earlier Hubble type. Absolute blue magnitude and absolute infrared magnitude are correlated for all Hubble types. The luminosity-log V(max) correlation has a slope 10 + or - 2 for each Hubble type, but with zero points displaced for each type.


* **Properties of the SN 1987A circumstellar ring and the distance to the Large Magellanic Cloud**: *Panagia, N. et al.* [1991ApJ...380L..23P](http://adsabs.harvard.edu/abs/1991ApJ...380L..23P) [1992ApJ...386L..31P](http://adsabs.harvard.edu/abs/1992ApJ...386L..31P)
> The distance to SN 1987A was determined by comparing the angular size of its circumstellar ring, measured from an HST image obtained in a narrow forbidden O III 5007 A filter, with its absolute size derived from an analysis of the light curves of narrow UV lines measured with the IUE. The analysis confirms that the observed elliptical structure is a circular ring seen at a mean inclination of i = 42.8 + or - 2.6 deg and provides a determination of its absolute diameter of (1.27 + or - 0.07) x 10 to the 18th cm. Its ratio to the angular diameter of 1.66 + or - 0.03 arcsec gives an accurate determination of the distance to SN 1987A, i.e., d(1987A) = 51.2 + or - 3.1 kpc.


* **Henry Cavendish, Johann von Soldner, and the deflection of light**: *Will, Clifford M.* [1988AmJPh..56..413W](http://adsabs.harvard.edu/abs/1988AmJPh..56..413W)
> The gravitational deflection of light based on Newtonian theory and the corpuscular model of light was calculated, but never published, around 1784 by Henry Cavendish, almost 20 years earlier than the first published calculation by Johann Georg von Soldner. The two results are slightly different because, while Cavendish treated a light ray emitted from infinity, von Soldner treated a light ray emitted from the surface of the gravitating body. At the first order of approximation, they agree with each other; both are one-half the value predicted by general relativity and confirmed by experiment.

